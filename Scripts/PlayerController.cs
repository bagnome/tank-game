﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Motor))]
public class PlayerController : MonoBehaviour {

	public float speed = 5.0f;
	private Motor motor;

	// Use this for initialization
	void Start () {
		motor = GetComponent<Motor> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey (KeyCode.A)) {
			motor.targetVel.x = -speed;
		} else if (Input.GetKey (KeyCode.D)) {
			motor.targetVel.x = speed;
		} else {
			motor.targetVel.x = 0;
		}
	}
}
