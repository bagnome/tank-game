using UnityEngine;
using System.Collections;

public class Turret : MonoBehaviour {
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		Vector2 coords = Camera.main.ScreenToWorldPoint (Input.mousePosition);
		Vector2 turret = this.rigidbody.position;

		float rotation;

		if (coords.x <= turret.x) {//is mouse to the left?
			if(coords.y <= turret.y){//is mouse below?
				rotation = Mathf.Atan(Mathf.Abs(coords.y - turret.y)/Mathf.Abs(coords.x - turret.x))*Mathf.Rad2Deg;
				print(rotation);
			}else{//mouse is above
				rotation = 270.0f + Mathf.Atan(Mathf.Abs(coords.x - turret.x)/Mathf.Abs(coords.y - turret.y))*Mathf.Rad2Deg;
				print(rotation);
			}
		}else{//mouse is to right
			if(coords.y <= turret.y){//is mouse below?
				rotation = 90.0f + Mathf.Atan(Mathf.Abs(coords.x - turret.x)/Mathf.Abs(coords.y - turret.y))*Mathf.Rad2Deg;
				print(rotation);
			}else{//mouse is above
				rotation = 180.0f + Mathf.Atan(Mathf.Abs(coords.y - turret.y)/Mathf.Abs(coords.x - turret.x))*Mathf.Rad2Deg;
				print(rotation);
			}
			//this.transform.rigidbody.rotation.z = rotation;
		
			transform.localEulerAngles = new Vector3(0,0,rotation);

		}
	}

}
